//Example commands to read/write from MODBUS device

//Initialisation
var modbus = require("modbus-serial");
//undefined
var client=new modbus()
//undefined
client.connectTCP("192.168.7.34", { port:502})
//Promise { <pending> }
client.setID(1)
//undefined

//Write 4 bytes
client.writeRegisters(314, [0,0x3f80])
//Read 2 registers (type REAL = 4 bytes)
client.readHoldingRegisters(208, 2).then(console.log)
// { data: [ 0, 16256 ], buffer: <Buffer 00 00 3f 80> }
//Read status (type INT = 2 bytes)
client.readHoldingRegisters(320, 1, function(err, data) {console.log(data);console.error(err)})
// Bytes to float
b=Buffer.from('0000803f', 'hex').readFloatLE()
b=Buffer.from('0080bb45', 'hex').readFloatLE()
//6000
