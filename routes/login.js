var express = require('express');
var crypto = require("crypto")
var router = express.Router();
const objUsers= require('../data/users.json')

// This will hold the users and authToken related to users
router.authTokens = {};

const generateAuthToken = () => {
    return crypto.randomBytes(30).toString('hex');
}
function getHashedPassword(pwd){
  return  crypto.createHmac("sha1",pwd).digest("hex") 
}

router.get('/ask', (req, res) => {
    res.render('login');
});

router.post("/submit", (req, res)=>{
    const { login, passwd,  } = req.body
    console.log(login, passwd, getHashedPassword(passwd) )
    console.log("users",objUsers)
    if (objUsers[login]==getHashedPassword(passwd)){
        token = generateAuthToken()
        global.loginRouter.authTokens[token]=login
        res.cookie("AuthToken", token)
        res.redirect("/marta/parameters")
    } else 
        res.render("error", {title: "Login error", error:{"message":"Bad login or password"}})
})
module.exports = router;
